console.log('Hello World');

function sum(num1, num2) {
	let result = num1 + num2;
	console.log(result);
}

function difference(num1, num2) {
	let result = num1 - num2;
	console.log(result);
}

console.log('Displayed sum of 5 and 15');

sum(5, 15);

console.log('Displayed difference of 20 and 5');

difference(20, 5);


function getProduct(num1, num2) {
	return num1 * num2;
}

function getQuotient(num1, num2) {
	return num1 / num2;
}

console.log('Displayed product of 50 and 10');

let product = getProduct(50, 10);

console.log(product);

console.log('Displayed quotient of 50 and 10');

let quotient = getQuotient(50, 10);

console.log(quotient);

function getCircleArea(radius) {
	return (Math.PI * (radius ** 2));
}

let circleArea = getCircleArea(15);

console.log('The result of getting the area of a circle with 15 radius: ');
console.log(circleArea);

function getAverageNumber(num1, num2, num3, num4) {
	let sum = num1 + num2 + num3 + num4;
	return (sum / 4)
}

let averageVar = getAverageNumber(20, 40, 60, 80);

console.log('The average of 20, 40, 60, 80: ');
console.log(averageVar);

function getPassingPercentage(score, totalScore) {
	let scorePercent = score / totalScore;
	let passingPercent = .75;
	let isPassed = scorePercent > passingPercent;

	return isPassed;
}

let isPassingScore = getPassingPercentage(38,50)

console.log('Is 38/50 a passing score?');
console.log(isPassingScore);